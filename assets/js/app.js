var $document = $(document), $element = $('.v-navbar-fixed'), className = 'show';
$document.scroll(function() {
    $element.toggleClass(className, $document.scrollTop() >= 300);
});

$(document).ready(function(){
    $('.slider-carousel').slick({
        infinite: true,
        slidesToShow: 1,
        dots: false,
        arrows: true,
        vertical: true,
        verticalSwiping: true,
        autoplay: true,
        autoplaySpeed: 6000,
        responsive: [
            {
              breakpoint: 544,
              settings: {
                arrows: false,
                vertical: false,
                verticalSwiping: false,
              }
            }
        ]
    });

    $('.testimonios-carousel').slick({
        infinite: true,
        slidesToShow: 2,
        dots: true,
        arrows: false,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
              }
            }
        ]
    });

    $('.servicios-carousel').slick({
        infinite: false,
        slidesToShow: 3,
        dots: false,
        arrows: false,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
              }
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 2,
              }
            }
        ]
    });

    $('.contacto-btn').click(function () {
        $(this).toggleClass('btn-primary btn-secondary');
        var option = $('#services ' + '#' + $(this).attr('id'));
        option.attr('selected') ? option.removeAttr('selected') : option.attr('selected', 'selected');
    });
});

(function($){
    $(window).on("load",function(){
        $(".v-navbar a, .v-navbar-fixed a, a[href='#top']").mPageScroll2id({
            scrollSpeed: 600,
            pageEndSmoothScroll: false,
            offset: 100
        });
    });
})(jQuery);
