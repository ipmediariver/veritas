<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reporte extends Model
{
    protected $table = 'reportes';

    protected $guarded = [];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
