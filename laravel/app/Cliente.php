<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'clientes';

    protected $guarded = [];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
