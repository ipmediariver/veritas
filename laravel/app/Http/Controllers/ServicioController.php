<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class ServicioController extends Controller
{
    public function index()
    {
        return view('site.servicios');
    }

    public function prueba_exploratoria()
    {
        return view('site.servicios.prueba-exploratoria');
    }

    public function prueba_permanencia()
    {
        return view('site.servicios.prueba-permanencia');
    }

    public function prueba_especifica()
    {
        return view('site.servicios.prueba-especifica');
    }

    public function prueba_psicometrica()
    {
        return view('site.servicios.prueba-psicometrica');
    }

    public function prueba_grafologica()
    {
        return view('site.servicios.prueba-grafologica');
    }

    public function prueba_eyedetect()
    {
        return view('site.servicios.prueba-eyedetect');
    }

    public function estudio_socioeconomico()
    {
        return view('site.servicios.estudio-socioeconomico');
    }
}
