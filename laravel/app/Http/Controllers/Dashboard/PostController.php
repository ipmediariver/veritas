<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Post;
use File;
class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post_list = Post::all();

        return view('dashboard.posts.index', compact('post_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->postFactory($request);

        return redirect()->route('dashboard.posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        return view('dashboard.posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->postFactory($request, $id);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        if ($post->image) {
            try{
                File::delete($post->image);
            }
            catch(\Exception $e){

            }
        }

        $post->delete();

        return back();
    }

    public function postFactory($request, $id = 0)
    {

        $post = Post::firstOrNew(['id' => $id]);

        $request['slug'] = str_slug($request->title);

        $this->postValidator($request, $post->id);

        $post->user_id = auth()->user()->id;
        $post->title = $request->title;
        $post->description = $request->description;
        $post->content = $request->content;

        if (!$id) {
            $post->slug = str_slug($request->title);
        }

        if ($request->image) {
            $image_name = pathinfo($request->image->getClientOriginalName(), PATHINFO_FILENAME);
            $image_ext = pathinfo($request->image->getClientOriginalName(), PATHINFO_EXTENSION);
            $image_fullname = date("Y_m_d_h_i_s_").str_slug($image_name, '_').'.'.$image_ext;

            $request->image->move('assets/uploads/posts', $image_fullname);

            if ($post->image) {
                try{
                    File::delete($post->image);
                }
                catch(\Exception $e){

                }
            }

            $post->image = 'assets/uploads/posts/'.$image_fullname;
        }

        $post->save();
    }

    public function postValidator($request, $id)
    {
        $this->validate($request, [
            'title' => "required|string|max:255|unique:posts,title,$id",
            'image' => 'image|max:5000',
            'description' => 'required|string',
            'content' => 'required|string',
            'slug' => "required|string|max:255|unique:posts,slug,$id",
        ]);
    }

    public function publish($id)
    {
        return back();
    }
}
