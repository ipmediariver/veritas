<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Reporte;
use App\User;
use DB;

class ReporteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reportes = DB::table('reportes')
            ->join('users', 'reportes.user_id', '=', 'users.id')
            ->join('clientes', 'users.id', '=', 'clientes.user_id')
            ->select('users.name', 'users.email', 'clientes.empresa', 'clientes.telefono', 'reportes.*')
            ->get();

        return view('dashboard.reportes.index')
            ->with('reportes', $reportes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('type', 'cliente')->get();

        return view('dashboard.reportes.create')
            ->with('usuarios', $users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Crear un reporte
        $reporte = new Reporte;

        $reporte->user_id = $request->user_id;

        $reporte->campo1 = $request->campo1;

        $reporte->campo2 = $request->campo2;

        $reporte->campo3 = $request->campo3;

        $reporte->campo4 = $request->campo4;

        $reporte->campo5 = $request->campo5;

        $reporte->campo6 = $request->campo6;

        $reporte->campo7 = $request->campo7;

        $reporte->campo8 = $request->campo8;

        $reporte->campo9 = $request->campo9;

        // Guardar datos
        $reporte->save();

        return redirect(route('dashboard.reportes'))
            ->with(['message' => 'Reporte guardado', 'class' => 'alert-info']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort('404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reporte = Reporte::find($id);

        $users = User::where('type', 'cliente')->get();

        return view('dashboard.reportes.edit')
            ->with(['reporte' => $reporte, 'usuarios' => $users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Obtener reporte
        $reporte = Reporte::find($id);

        $reporte->user_id = $request->user_id;

        $reporte->campo1 = $request->campo1;

        $reporte->campo2 = $request->campo2;

        $reporte->campo3 = $request->campo3;

        $reporte->campo4 = $request->campo4;

        $reporte->campo5 = $request->campo5;

        $reporte->campo6 = $request->campo6;

        $reporte->campo7 = $request->campo7;

        $reporte->campo8 = $request->campo8;

        $reporte->campo9 = $request->campo9;

        // Guardar datos
        $reporte->save();

        return back()
            ->with(['message' => 'Cambios guardados', 'class' => 'alert-info']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Reporte::destroy($id);
        return redirect(route('dashboard.reportes'))
            ->with(['message' => 'Reporte eliminado', 'class' => 'alert-info']);
    }

}
