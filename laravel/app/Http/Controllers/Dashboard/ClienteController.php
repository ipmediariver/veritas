<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Cliente;
use App\User;
use DB;

class ClienteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = DB::table('clientes')
            ->join('users', 'clientes.user_id', '=', 'users.id')
            ->select('users.name', 'users.email', 'clientes.*')
            ->get();

        return view('dashboard.clientes.index')
            ->with('clientes', $clientes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Crear un ususario
        $user = new User;

        $user->name = $request->name;

        $user->email = $request->email;

        $user->password = bcrypt($request->password);

        $user->type = 'cliente';

        // Guardar datos
        $user->save();

        // Crear un cliente
        $cliente = new Cliente;

        $cliente->user_id = $user->id;

        $cliente->empresa = $request->empresa;

        $cliente->telefono = $request->telefono;

        $cliente->campo1 = $request->campo1;

        $cliente->campo2 = $request->campo2;

        $cliente->campo3 = $request->campo3;

        $cliente->campo4 = $request->campo4;

        $cliente->campo5 = $request->campo5;

        $cliente->campo6 = $request->campo6;

        $cliente->campo7 = $request->campo7;

        $cliente->campo8 = $request->campo8;

        $cliente->campo9 = $request->campo9;

        // Guardar datos
        $cliente->save();

        // Regreasar a la vista de clientes
        return redirect(route('dashboard.clientes'))
            ->with(['message' => 'Cliente guardado', 'class' => 'alert-info']);;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort('404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = DB::table('clientes')
            ->join('users', 'clientes.user_id', '=', 'users.id')
            ->select('users.name', 'users.email', 'clientes.*')
            ->where('clientes.id', $id)
            ->first();

        // return dd($cliente);
        return view('dashboard.clientes.edit')
            ->with('cliente', $cliente);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Obtener cliente
        $cliente = Cliente::find($id);

        $cliente->empresa = $request->empresa;

        $cliente->telefono = $request->telefono;

        $cliente->campo1 = $request->campo1;

        $cliente->campo2 = $request->campo2;

        $cliente->campo3 = $request->campo3;

        $cliente->campo4 = $request->campo4;

        $cliente->campo5 = $request->campo5;

        $cliente->campo6 = $request->campo6;

        $cliente->campo7 = $request->campo7;

        $cliente->campo8 = $request->campo8;

        $cliente->campo9 = $request->campo9;

        // Guardar datos
        $cliente->save();

        // Obtener ususario
        $user = User::where('id', $cliente->user_id)->first();

        $user->name = $request->name;

        $user->email = $request->email;

        // Guardar datos
        $user->save();

        //Regresar a la vista anterior
        return back()
            ->with(['message' => 'Cambios guardados', 'class' => 'alert-info']);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_id = Cliente::find($id)['user_id'];

        Cliente::destroy($id);

        User::destroy($user_id);

        return redirect(route('dashboard.clientes'))
            ->with(['message' => 'Cliente eliminado', 'class' => 'alert-info']);;
    }

}
