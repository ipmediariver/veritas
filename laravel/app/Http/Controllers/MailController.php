<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class MailController extends Controller
{
    public function contacto(Request $request)
    {
        // return dd($request->input());
        try {
            // Asignar request a la variable r
            $r = $request;
            // Recoger datos recibidos en un arreglo
            $data = [
                'nombre'    => $r->input('name'),
                'correo'    => $r->input('email'),
                'empresa'   => $r->input('company'),
                'telefono'  => $r->input('phone'),
                'fecha'     => $r->input('date'),
                'web'       => $r->input('web'),
                'mensaje'   => $r->input('message'),
                'servicios' => $r->input('services')
            ];

            if (empty($data['servicios'])) {
                return back()->withInput()->with(['message' => 'Mensaje no enviado, seleccione al menos un servicio.', 'class' => 'alert-danger']);
            }

            // Enviar correo
            Mail::send('emails.contacto', $data, function($message)
            {
                $message->subject('Solicitud de servicio');
                $message->from('godynacom@gmail.com', 'Centro Veritas de Control y Confianza');
                // $message->to('andresmichelgonzalez@gmail.com', 'Andrés');
                $message->to('afarfan@godynacom.com', 'Andrés Farfán');
                // $message->cc('andresmichelgonzalez@gmail.com');
            });
        }
        catch (\Exception $e) {
            // return back()->withInput()->with(['message' => $e->getMessage(), 'class' => 'alert-danger']);
            return back()->withInput()->with(['message' => 'Mensaje no enviado, intente de nuevo.', 'class' => 'alert-danger']);
        }

        return back()->with(['message' => 'Mensaje enviado', 'class' => 'alert-info']);
    }
}
