<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Post;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::paginate(5);

        $other_posts = Post::all();

        return view('site.blog.index', compact('posts', 'other_posts'));
    }

    public function post($slug)
    {
        $post = Post::where('slug', $slug)->first();

        $other_posts = Post::where('id', '!=', $post->id)->get();

        return view('site.blog.post', compact('post', 'other_posts'));
    }
}
