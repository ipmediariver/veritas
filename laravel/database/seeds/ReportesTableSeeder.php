<?php

use Illuminate\Database\Seeder;

class ReportesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reportes')->insert([
            'id' => 1,
            'user_id' => 2,
            'campo1' => 'Campo 1',
            'campo2' => 'Campo 2',
            'campo3' => 'Campo 3',
            'campo4' => 'Campo 4',
            'campo5' => 'Campo 5',
            'campo6' => 'Campo 6',
            'campo7' => 'Campo 7',
            'campo8' => 'Campo 8',
            'campo9' => 'Campo 9',
        ]);
    }
}
