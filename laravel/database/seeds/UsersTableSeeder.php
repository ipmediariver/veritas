<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Andrés Farfán',
            'email' => 'admin@centroveritas.com',
            'password' => bcrypt('secret'),
            'type' => 'admin',
        ]);

        DB::table('users')->insert([
            'id' => 2,
            'name' => 'Andrés Michel',
            'email' => 'amichel@ipmediariver.com',
            'password' => bcrypt('secret'),
            'type' => 'cliente',
        ]);
    }
}
