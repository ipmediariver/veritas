
<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        //DB::table('users')->delete();

        // UsersTableSeeder
        User::create(array(
            'name'     => 'Andrés Farfán',
            'email'    => 'admin@centroveritas.com',
            'password' => Hash::make('secret'),
        ));
    }
}