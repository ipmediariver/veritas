<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Centro Veritas de Control y Confianza</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/png" href="img/favicon.png" />
		<link rel="stylesheet" href="{{asset('assets/template/css/style.css')}}">
		<link rel="stylesheet" href="{{asset('assets/template/css/responsive.css')}}">
	</head>
	<body>
		<header id="header" class="stricky">
			<div class="container">
				<div class="logo">
					<a href="index.html">
						<img src="{{asset('assets/template/img/logo_veritas.svg')}}" alt="Centro Veritas de Control y Confianza"/>
					</a>
				</div>
				<nav class="mainmenu-navigation">
					<div class="navigation pull-right">
						<div class="nav-header">
							<button><i class="fa fa-bars"></i></button>
						</div>
						<div class="nav-footer">
							<ul class="nav">
								<li class="active"><a href="index.html">Inicio</a></li>
								<li><a href="#">Nosotros</a></li>
								<li><a href="#">Servicios</a></li>
								<li><a href="#">Contacto</a></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
		</header>
		<section class="rev_slider_wrapper gardener-banner">
			<div id="slider1" class="rev_slider"  data-version="5.0">
				<ul>
					<li data-transition="parallaxvertical">
						<img src="{{asset('assets/template/img/banner_1.jpg')}}"  alt="" width="1600" height="450" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
						<div class="tp-caption sfl tp-resizeme gardener-caption-h1 font-light" data-x="left" data-hoffset="0" data-y="top" data-voffset="80" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="500">
							Seguridad Corporativa
						</div>
						<div class="tp-caption sfr tp-resizeme gardener-caption-h1 bg-white" data-x="left" data-hoffset="0" data-y="top" data-voffset="150" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="1000">
							<span>Lorem ipsum ad his scripta blandit partiendo</span>
						</div>
						<div class="tp-caption sfb tp-resizeme gardener-caption-p" data-x="left" data-hoffset="0" data-y="top" data-voffset="234" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="1500">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed <br>do eiusmod tempor incididunt ut labore et dolore magna <br>aliqua. Ut enim ad minim veniam.
						</div>
						<div class="tp-caption sfb tp-resizeme" data-x="left" data-hoffset="0" data-y="top" data-voffset="340" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="2000">
							<a href="#" class="banner-btn">Continuar</a>
						</div>
					</li>
					<li data-transition="parallaxvertical">
						<img src="{{asset('assets/template/img/banner_1.jpg')}}"  alt=""  width="1600" height="450" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="2" >
						<div class="tp-caption sft tp-resizeme gardener-caption-h1 font-light" data-x="left" data-hoffset="0" data-y="top" data-voffset="80" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="500">
							Corporaciones de Seguridad
						</div>
						<div class="tp-caption sft tp-resizeme gardener-caption-h1 bg-white" data-x="left" data-hoffset="0" data-y="top" data-voffset="150" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="1000">
							<span class="bold">Lorem ipsum ad his scripta</span>
						</div>
						<div class="tp-caption sfb tp-resizeme gardener-caption-p" data-x="left" data-hoffset="0" data-y="top" data-voffset="234" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="1500">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed <br>do eiusmod tempor incididunt ut labore et dolore magna <br>aliqua. Ut enim ad minim veniam.
						</div>
						<div class="tp-caption sfb tp-resizeme" data-x="left" data-hoffset="0" data-y="top" data-voffset="340" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="2000">
							<a href="#" class="banner-btn">Continuar</a>
						</div>
					</li>
					<li data-transition="parallaxvertical">
						<img src="{{asset('assets/template/img/banner_1.jpg')}}"  alt=""  width="1600" height="450" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="2" >
						<div class="tp-caption sft tp-resizeme gardener-caption-h1 font-light" data-x="left" data-hoffset="0" data-y="top" data-voffset="80" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="500">
							Recursos Humanos
						</div>
						<div class="tp-caption sft tp-resizeme gardener-caption-h1 bg-white" data-x="left" data-hoffset="0" data-y="top" data-voffset="150" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="1000">
							<span class="bold">Lorem ipsum ad his</span>
						</div>
						<div class="tp-caption sfb tp-resizeme gardener-caption-p" data-x="left" data-hoffset="0" data-y="top" data-voffset="234" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="1500">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed <br>do eiusmod tempor incididunt ut labore et dolore magna <br>aliqua. Ut enim ad minim veniam.
						</div>
						<div class="tp-caption sfb tp-resizeme" data-x="left" data-hoffset="0" data-y="top" data-voffset="340" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="2000">
							<a href="#" class="banner-btn">Continuar</a>
						</div>
					</li>
				</ul>
			</div>
		</section>
		<section id="more-services">
			<div class="container">
				<div class="section-title-style text-center">
					<h1>Nuestros Servicios</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur.
					</p>
				</div>
			</div>
		</section>
		<section id="aboutus">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-12 pull-right">
						<div class="about-content">
							<h1>Quienes Somos</h1>
							<p>
								Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
							</p>
							<br>
							<p class="padd-20">
								Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
							</p>
							<a class="btn btn-default-lg btn-lg">Continuar Leyendo</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="testimonials">
			<div class="container">
				<div class="section-title-style text-center">
					<h1>Testimonios de Clientes</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididtion ullamco luptate velit esse fugiat nulla pariatur.
					</p>
				</div>
				<div class="row">
					<div class="col-lg-12 col-sm-12">
						<div class="owl-carousel">
							<div class="item">
								<div class="quote-icon">
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</div>
								<div class="text-box clearfix">
									<p>
										Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis Red quia numquam eius modi. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
									</p>
								</div>
								<div class="client-img">
									<img src="http://placehold.it/90" style="border-radius: 50%" alt="client-img">
								</div>
								<p class="name">
									<span>Andrés Farfán </span> <br> ( c e o )
								</p>
							</div>
							<div class="item">
								<div class="quote-icon">
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</div>
								<div class="text-box clearfix">
									<p>
										Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis Red quia numquam eius modi. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
									</p>
								</div>
								<div class="client-img">
									<img src="http://placehold.it/90" style="border-radius: 50%" alt="client-img">
								</div>
								<p class="name">
									<span>Agustín Abaroa </span> <br> ( c e o )
								</p>
							</div>
							<div class="item">
								<div class="quote-icon">
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</div>
								<div class="text-box clearfix">
									<p>
										Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis Red quia numquam eius modi. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
									</p>
								</div>
								<div class="client-img">
									<img src="http://placehold.it/90" style="border-radius: 50%" alt="client-img">
								</div>
								<p class="name">
									<span>Andrés Farfán </span> <br> ( c e o )
								</p>
							</div>
							<div class="item">
								<div class="quote-icon">
									<i class="fa fa-quote-left" aria-hidden="true"></i>
								</div>
								<div class="text-box clearfix">
									<p>
										Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis Red quia numquam eius modi. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
									</p>
								</div>
								<div class="client-img">
									<img src="http://placehold.it/90" style="border-radius: 50%" alt="client-img">
								</div>
								<p class="name">
									<span>Agustín Abaroa </span> <br> ( c e o )
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="clients">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="owl-carousel">
							<div class="item">
								<div class="client-img-holder">
									<img src="img/clients/1.jpg" alt="client">
								</div>
							</div>
							<div class="item">
								<div class="client-img-holder">
									<img src="img/clients/2.jpg" alt="client">
								</div>
							</div>
							<div class="item">
								<div class="client-img-holder">
									<img src="img/clients/3.jpg" alt="client">
								</div>
							</div>
							<div class="item">
								<div class="client-img-holder">
									<img src="img/clients/4.jpg" alt="client">
								</div>
							</div>
							<div class="item">
								<div class="client-img-holder">
									<img src="img/clients/2.jpg" alt="client">
								</div>
							</div>
							<div class="item">
								<div class="client-img-holder">
									<img src="img/clients/1.jpg" alt="client">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<div class="footer-widget">
							<h3>Centro Veritas</h3>
							<p>
								Lorem ipsum dolor sit amet, consect etur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ali qua. Ut enim ad minim veniam, quis no strud.
							</p>
							<a class="readmore" href="#">Leer más</a>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
						<div class="footer-widget padd-offset">
							<h3>Menú</h3>
							<ul class="quick-links">
								<li><a href="#">Nosotros</a></li>
								<li><a href="#">Servicios</a></li>
								<li><a href="#">Ayuda</a></li>
								<li><a href="#">Políticas de privacidad</a></li>
								<li><a href="#">Contacto</a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
						<div class="footer-widget">
							<h3>Servicios</h3>
							<ul class="quick-links">
								<li><a href="#">Seguridad Corporativa</a></li>
								<li><a href="#" style="white-space:nowrap">Corporaciones de Seguridad</a></li>
								<li><a href="#">Recursos Humanos</a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="footer-widget padd-offset-1">
							<h3>Comunícate con Nosotros</h3>
							<div class="textwidget">
								<div class="textwidget-icon">
									<i class="fa fa-map-marker"></i>
								</div>
								<div class="info-detail">
									<p>
										Lorem 7845 Consectetur
									</p>
								</div>
							</div>
							<div class="textwidget">
								<div class="textwidget-icon">
									<i class="fa fa-phone"></i>
								</div>
								<div class="info-detail">
									<p>
										(664) 124 7845
									</p>
								</div>
							</div>
							<div class="textwidget">
								<div class="textwidget-icon">
									<i class="fa fa-envelope"></i>
								</div>
								<div class="info-detail">
									<p>
										info@centroveritas.com
									</p>
								</div>
							</div>
							<div class="textwidget">
								<div class="textwidget-icon">
									<i class="fa fa-clock-o"></i>
								</div>
								<div class="info-detail">
									<p>
										Luneas - Viernes: 9:00AM - 5:00PM
										Sábado - Domingo: Cerrado
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="create-by">
								<p>Desarrollo de: IP Media River</p>
							</div>
							<div class="copyright">
								<p>
									© 2016, Centro Veritas de Control y Confianza.
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<script src="{{asset('assets/template/js/jquery.min.js')}}"></script>
		<script src="{{asset('assets/template/js/bootstrap.min.js')}}"></script>
		<script src="{{asset('assets/template/js/wow.js')}}"></script>
		<script src="{{asset('assets/template/js/jquery.appear.js')}}"></script>
		<script src="{{asset('assets/template/js/jquery.countTo.js')}}"></script>
		<script src="{{asset('assets/template/js/owl.carousel.min.js')}}"></script>
		<script src="{{asset('assets/template/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
		<script src="{{asset('assets/template/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
		<script src="{{asset('assets/template/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
		<script src="{{asset('assets/template/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
		<script src="{{asset('assets/template/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
		<script src="{{asset('assets/template/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
		<script src="{{asset('assets/template/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
		<script src="{{asset('assets/template/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
		<script src="{{asset('assets/template/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
		<script src="{{asset('assets/template/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
		<script src="{{asset('assets/template/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
		<script src="{{asset('assets/template/js/custom.js')}}"></script>
	</body>
</html>
