@extends('layouts.dashboard')

@section('title')

    Dashobard | Artículos

@endsection

@section('posts-view', 'active')

@section('section-title')
    <div class="page-title">
        <div class="title">Artículos</div>
        <div class="sub-title">Editar artículo</div>
    </div>
@endsection

@section('content')
    <div class="card bg-white">
        <div class="card-header">
            Formulario de artículos
        </div>

        <div class="card-block">
            <div class="row m-a-0">
                <div class="col-lg-12">
                    <form class="form-horizontal" action="{{ route('posts.update', $post->id) }}" method="post" enctype="multipart/form-data">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-6">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Título</label>
                            <div class="col-sm-6">
                                <input type="text" name="title" value="{{ $post->title }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Portada</label>
                            <div class="col-sm-6">
                                <input type="file" name="image" accept="image/x-png,image/jpeg" class="form-control input-rounded">
                                <p class="help-block">
                                    <a href="{{ asset($post->image) }}" target="_blank" style="text-decoration:underline">
                                        Ver portada actual
                                    </a>
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descripción</label>
                            <div class="col-sm-6">
                                <textarea name="description" rows="2" cols="80" class="form-control input-rounded" required="">{{ $post->description }}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Contenido</label>
                            <div class="col-sm-6">
                                <textarea name="content" rows="10" cols="80" class="form-control input-rounded bootstrap-wysiwyg" required>{!! $post->content !!}</textarea>
                            </div>
                        </div>


                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-6">
                            <button class="m-t btn btn-primary" type="submit">Guardar cambios</button>
                        </div>
                    </form>

                    {{-- <form action="{{ route('clientes.destroy', $cliente->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <button type="submit">Delete</button>
                    </form> --}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        (function($){'use strict';$('.bootstrap-wysiwyg').wysihtml5({toolbar:{fa:true}});})(jQuery);
    </script>
@endsection
