@extends('layouts.dashboard')

@section('title')
    Dashobard | Artículos
@endsection

@section('posts-view', 'active')

@section('section-title')
    <div class="page-title">
        <div class="title">Artículos</div>
        <div class="sub-title">Lista de artículos</div>
    </div>
@endsection

@section('content')
<div class="card bg-white">
    <div class="card-header">
        Tabla de artículos
    </div>
    <div class="card-block">
        <table class="m-t table table-bordered table-striped datatable editable-datatable responsive align-middle bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Título</th>
                    <th>Descripción</th>
                    <th width="80">Fecha</th>
                    <th>Compartir</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($post_list as $post)
                    <tr>
                        <td>{{ $post->id }}</td>
                        <td>{{ $post->title }}</td>
                        <td>{{ str_limit($post->description) }}</td>
                        <td>{{ str_limit($post->created_at, 10, '') }}</td>
                        <td>
                            <div class="fb-share-button" data-href="{{ route('blog.post', $post->slug) }}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ route('blog.post', $post->slug) }};src=sdkpreparse">Compartir</a></div>
                        </td>
                        <td>
                            <a href="{{ route('posts.edit', $post->id) }}">
                                Editar
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('posts.delete', $post->id) }}">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $('.dataTables_length').prepend(`<a href="{{ route('posts.create') }}" class='btn btn-primary m-r'>Agregar artículo</a>`);
    </script>
@endsection
