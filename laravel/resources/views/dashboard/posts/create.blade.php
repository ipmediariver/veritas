@extends('layouts.dashboard')

@section('title')

    Dashobard | Artículos

@endsection

@section('posts-view', 'active')

@section('section-title')
    <div class="page-title">
        <div class="title">Artículos</div>
        <div class="sub-title">Agregar artículo</div>
    </div>
@endsection

@section('content')
    <div class="card bg-white">
        <div class="card-header">
            Formulario de artículos
        </div>

        <div class="card-block">
            <div class="row m-a-0">
                <div class="col-lg-12">
                    <form class="form-horizontal" action="{{ route('posts.store') }}" method="post" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-6">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Título</label>
                            <div class="col-sm-6">
                                <input type="text" name="title" value="{{ old('title') }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Portada</label>
                            <div class="col-sm-6">
                                <input type="file" name="image" accept="image/x-png,image/jpeg" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descripción</label>
                            <div class="col-sm-6">
                                <textarea name="description" rows="2" cols="80" class="form-control input-rounded" required="">{{ old('description') }}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Contenido</label>
                            <div class="col-sm-6">
                                <textarea name="content" rows="10" cols="80" class="form-control input-rounded bootstrap-wysiwyg" required>{!! old('content') !!}</textarea>
                            </div>
                        </div>


                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-6">
                            <button class="m-t btn btn-primary" type="submit">Agregar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        (function($){'use strict';$('.bootstrap-wysiwyg').wysihtml5({toolbar:{fa:true}});})(jQuery);
    </script>
@endsection
