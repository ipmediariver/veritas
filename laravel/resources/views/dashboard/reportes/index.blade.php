@extends('layouts.dashboard')

@section('title')
    Dashobard | Reportes
@endsection

@section('reportes-view', 'active')

@section('section-title')
    <div class="page-title">
        <div class="title">Reportes</div>
        <div class="sub-title">Lista de reportes</div>
    </div>
@endsection

@section('content')
<div class="card bg-white">
    <div class="card-header">
        Tabla de reportes
    </div>
    <div class="card-block">
        <table class="m-t table table-bordered table-striped datatable editable-datatable responsive align-middle bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Empresa</th>
                    <th>Telefono</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($reportes as $reporte)
                    <tr>
                        <td>{{ $reporte->id }}</td>
                        <td>{{ $reporte->name }}</td>
                        <td>{{ $reporte->email }}</td>
                        <td>{{ $reporte->empresa }}</td>
                        <td>{{ $reporte->telefono }}</td>
                        <td>
                            <a href="{{ route('reportes.edit', $reporte->id) }}">
                                Editar
                           </a>
                        </td>
                        <td>
                            <a href="{{ route('reportes.delete', $reporte->id) }}">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $('.dataTables_length').prepend(`<a href="{{ route('reportes.create') }}" class='btn btn-primary m-r'>Agregar reporte</a>`);
    </script>
@endsection
