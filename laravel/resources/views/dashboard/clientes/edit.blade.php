@extends('layouts.dashboard')

@section('title')

    Dashobard | Clientes

@endsection

@section('clientes-view', 'active')


@section('section-title')
    <div class="page-title">
        <div class="title">Clientes</div>
        <div class="sub-title">Editar cliente</div>
    </div>
@endsection

@section('content')

    <div class="card bg-white">
        <div class="card-header">
            Formulario de cliente
        </div>
        <div class="card-block">
            <div class="row m-a-0">
                <div class="col-lg-12">
                    <form class="form-horizontal" action="{{ route('clientes.update', $cliente->id) }}" method="post" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        {{ method_field('PUT') }}

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nombre</label>
                            <div class="col-sm-4">
                                <input type="text" name="name" value="{{ $cliente->name }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Correo electrónico</label>
                            <div class="col-sm-4">
                                <input type="email" name="email" value="{{ $cliente->email }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        {{-- <div class="form-group">
                            <label class="col-sm-2 control-label">Contraseña</label>
                            <div class="col-sm-4">
                                <input type="password" name="password" value="" class="form-control input-rounded" required="">
                            </div>
                        </div> --}}

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Empresa</label>
                            <div class="col-sm-4">
                                <input type="text" name="empresa" value="{{$cliente->empresa }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Telefono</label>
                            <div class="col-sm-4">
                                <input type="phone" name="telefono" value="{{ $cliente->telefono }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Dirección</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo1" value="{{ $cliente->campo1 }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Género</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo1" value="{{ $cliente->campo1 }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 2</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo2" value="{{ $cliente->campo2 }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 3</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo3" value="{{ $cliente->campo3 }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 4</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo4" value="{{ $cliente->campo4 }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 5</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo5" value="{{ $cliente->campo5 }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 6</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo6" value="{{ $cliente->campo6 }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 7</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo7" value="{{ $cliente->campo7 }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 8</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo8" value="{{ $cliente->campo8 }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 9</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo9" value="{{ $cliente->campo9 }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">
                            <button class="m-t btn btn-primary" type="submit">Agregar</button>
                        </div>

                    </form>

                    {{-- <form action="{{ route('clientes.destroy', $cliente->id) }}" method="post">

                        {{ csrf_field() }}

                        {{ method_field('DELETE') }}

                        <button type="submit">Delete</button>

                    </form> --}}

                </div>
            </div>
        </div>
    </div>

@endsection
