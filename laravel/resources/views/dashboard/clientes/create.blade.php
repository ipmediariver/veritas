@extends('layouts.dashboard')

@section('title')

    Dashobard | Clientes

@endsection

@section('clientes-view', 'active')

@section('section-title')
    <div class="page-title">
        <div class="title">Clientes</div>
        <div class="sub-title">Agregar cliente</div>
    </div>
@endsection

@section('content')

    <div class="card bg-white">
        <div class="card-header">
            Formulario de cliente
        </div>
        <div class="card-block">
            <div class="row m-a-0">
                <div class="col-lg-12">
                    <form class="form-horizontal" action="{{ route('clientes.store') }}" method="post" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nombre</label>
                            <div class="col-sm-4">
                                <input type="text" name="name" value="{{ old('name') }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Correo electrónico</label>
                            <div class="col-sm-4">
                                <input type="text" name="email" value="{{ old('email') }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Contraseña</label>
                            <div class="col-sm-4">
                                <input type="password" name="password" value="" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Empresa</label>
                            <div class="col-sm-4">
                                <input type="text" name="empresa" value="{{ old('empresa') }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Telefono</label>
                            <div class="col-sm-4">
                                <input type="phone" name="telefono" value="{{ old('telefono') }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Dirección</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo1" value="{{ old('campo1') }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Género</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo1" value="{{ old('campo1') }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 2</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo2" value="{{ old('campo2') }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 3</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo3" value="{{ old('campo3') }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 4</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo4" value="{{ old('campo4') }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 5</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo5" value="{{ old('campo5') }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 6</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo6" value="{{ old('campo6') }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 7</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo7" value="{{ old('campo7') }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 8</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo8" value="{{ old('campo8') }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campo 9</label>
                            <div class="col-sm-4">
                                <input type="input" name="campo9" value="{{ old('campo9') }}" class="form-control input-rounded" required="">
                            </div>
                        </div>

                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">
                            <button class="m-t btn btn-primary" type="submit">Agregar</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
