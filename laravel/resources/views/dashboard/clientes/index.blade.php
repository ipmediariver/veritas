@extends('layouts.dashboard')

@section('title')
    Dashobard | Clientes
@endsection

@section('clientes-view', 'active')

@section('section-title')
    <div class="page-title">
        <div class="title">Clientes</div>
        <div class="sub-title">Lista de clientes</div>
    </div>
@endsection

@section('content')
<div class="card bg-white">
    <div class="card-header">
        Tabla de clientes
    </div>
    <div class="card-block">
        <table class="m-t table table-bordered table-striped datatable editable-datatable responsive align-middle bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Empresa</th>
                    <th>Telefono</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clientes as $cliente)
                    <tr>
                        <td>{{ $cliente->id }}</td>
                        <td>{{ $cliente->name }}</td>
                        <td>{{ $cliente->email }}</td>
                        <td>{{ $cliente->empresa }}</td>
                        <td>{{ $cliente->telefono }}</td>
                        <td>
                            <a href="{{ route('clientes.edit', $cliente->id) }}">
                                Editar
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('clientes.delete', $cliente->id) }}">
                                Eliminar
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $('.dataTables_length').prepend(`<a href="{{ route('clientes.create') }}" class='btn btn-primary m-r'>Agregar cliente</a>`);
    </script>
@endsection
