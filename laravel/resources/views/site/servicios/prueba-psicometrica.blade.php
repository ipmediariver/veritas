@extends('layouts.section')

@section('title', 'Prueba psocométrica')

@section('breadcrumb-title', 'Prueba psocométrica')

@section('breadcrumb-list')
    <li class="breadcrumb-item"><a href="{{ url('/') }}">Inicio</a></li>
    <li class="breadcrumb-item"><a href="{{ route('servicios') }}">Servicios</a></li>
    <li class="breadcrumb-item active">Prueba psicométrica</li>
@endsection

@section('section-content')
    <div class="section-cover-container container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section-cover" style="background-image: url({{ asset('assets/img/pruebas/prueba-psicometrica.jpg') }})"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-container container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <p>
                        Son las evaluaciones utilizadas para identificar determinados valores, rasgos y competencias que ayudan a darle al entrevistador una idea más clara, creando un perfil psicológico del aspirante con la finalidad de elegir al candidato idóneo a un puesto.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="offset-xs-0 col-xs-12 offset-sm-1 col-sm-10 offset-lg-2 col-lg-8 servicios-container flex flex-col">
                <h3 class="text-xs-center m-b-3">Más de nuestros servicios</h3>
            </div>
        </div>
    </div>

    <div class="servicios-carousel-div container-fluid">
        <div class="container">
            <div class="row servicios-carousel">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="card m-t-1">
                        <div class="card-img" style="background-image: linear-gradient(to bottom, rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url({{ asset('assets/img/pruebas/prueba-exploratoria.jpg') }})">
                            <h3>Prueba exploratoria</h3>
                        </div>
                        <p class="card-text">
                            {{ str_limit('Facilita los procesos de selección de personal determinado de manera objetiva la honestidad y confiabilidad de los candidatos.', 110) }}
                        </p>
                        <div class="card-btn-container">
                            <a class="btn btn-secondary" href="{{ route('prueba.exploratoria') }}">Leer más</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="card m-t-1">
                        <div class="card-img" style="background-image: linear-gradient(to bottom, rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url({{ asset('assets/img/pruebas/prueba-permanencia-laboral.jpg') }})">
                            <h3>Prueba de permanencia laboral</h3>
                        </div>
                        <p class="card-text">
                            {{ str_limit('Permite que las empresas evalúen constantemente al personal, para evitar que cometan acciones que van en contra de las normas, políticas y procedimientos de la empresa. Y así garantizar la confiabilidad en el desempeño del cargo.', 110) }}
                        </p>
                        <div class="card-btn-container">
                            <a class="btn btn-secondary" href="{{ route('prueba.permanencia') }}">Leer más</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="card m-t-1">
                        <div class="card-img" style="background-image: linear-gradient(to bottom, rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url({{ asset('assets/img/pruebas/prueba-especifica.jpg') }})">
                            <h3>Prueba específica</h3>
                        </div>
                        <p class="card-text">
                            {{ str_limit('Se lleva a cabo cuando se tiene un hecho en concreto, sobre el cual se requiere valorar la veracidad del testimonio de la persona, normalmente el tema está relacionado con la presunta responsabilidad o involucramiento del sujeto.', 110) }}
                        </p>
                        <div class="card-btn-container">
                            <a class="btn btn-secondary" href="{{ route('prueba.especifica') }}">Leer más</a>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="card m-t-1">
                        <div class="card-img" style="background-image: linear-gradient(to bottom, rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url({{ asset('assets/img/pruebas/prueba-psicometrica.jpg') }})">
                            <h3>Pruebas psicométricas</h3>
                        </div>
                        <p class="card-text">
                            {{ str_limit('Son las evaluaciones utilizadas para identificar determinados valores, rasgos y competencias que ayudan a darle al entrevistador una idea más clara, creando un perfil psicológico del aspirante con la finalidad de elegir al candidato idóneo a un puesto.', 110) }}
                        </p>
                        <div class="card-btn-container">
                            <a class="btn btn-secondary" href="{{ route('prueba.psicometrica') }}">Leer más</a>
                        </div>
                    </div>
                </div> -->
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="card m-t-1">
                        <div class="card-img" style="background-image: linear-gradient(to bottom, rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url({{ asset('assets/img/pruebas/prueba-grafologica.jpg') }})">
                            <h3>Prueba grafológica</h3>
                        </div>
                        <p class="card-text">
                            {{ str_limit('Permite obtener un conocimiento profundo de la personalidad del individuo, es una prueba proyectiva basada en la interpretación de elementos en los que la personalidad del analizado se proyecta.', 110) }}
                        </p>
                        <div class="card-btn-container">
                            <a class="btn btn-secondary" href="{{ route('prueba.grafologica') }}">Leer más</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="card m-t-1">
                        <div class="card-img" style="background-image: linear-gradient(to bottom, rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url({{ asset('assets/img/pruebas/prueba-eyedetect.jpg') }})">
                            <h3>Pruebas eyedetect</h3>
                        </div>
                        <p class="card-text">
                            {{ str_limit('Una tecnología precisa y no invasiva que detecta mentiras por medio del análisis del comportamiento de los ojos. EyeDetect permite a las empresas contratar y mantener fuerza de trabajo confiable y honesta.', 110) }}
                        </p>
                        <div class="card-btn-container">
                            <a class="btn btn-secondary" href="{{ route('prueba.eyedetect') }}">Leer más</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="card m-t-1">
                        <div class="card-img" style="background-image: linear-gradient(to bottom, rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url({{ asset('assets/img/pruebas/estudio-socioeconomico.jpg') }})">
                            <h3>Estudios socioeconomico</h3>
                        </div>
                        <p class="card-text">
                            {{ str_limit('Permite a su empresa verificar y analizar la veracidad de la informacion personal y laboral presentada por cada candidato potencial a ocupar una plaza dentro de su organización.', 110) }}
                        </p>
                        <div class="card-btn-container">
                            <a class="btn btn-secondary" href="{{ route('estudio.socioeconomico') }}">Leer más</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
