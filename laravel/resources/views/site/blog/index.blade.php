@extends('layouts.section')

@section('title', 'Blog')

@section('breadcrumb-title', 'Blog')

@section('breadcrumb-list')
    <li class="breadcrumb-item"><a href="{{ url('/') }}">Inicio</a></li>
    <li class="breadcrumb-item active">Blog</li>
@endsection

@section('section-content')
    <div class="servicios" id="servicios" style="background-image:url();">
        <div class="container">
            <div class="row">
                <div class="col-xs-9">
                    @if (count($posts))
                        @foreach ($posts as $post)
                            <div class="row">
                                <div class="col-xs-12">
                                    @if ($loop->first)
                                        <div class="card">
                                    @else
                                        <div class="card m-t-1">
                                    @endif
                                        <div class="card-text p-b-2" style="height:auto;position:relative">
                                            <p>{{ str_limit($post->created_at, 10, '') }}</p>
                                            <h3>
                                                <a href="{{ route('blog.post', $post->slug) }}" class="clean">
                                                    {{ str_limit($post->title, 110) }}
                                                </a>
                                            </h3>
                                        </div>

                                        <div class="card-img" style="margin-left:30px;margin-right:30px;background-image:url({{ asset($post->image) }})"></div>

                                        <p class="card-text p-b-0" style="height:auto;position:relative;font-size:16px;">
                                            {{ str_limit($post->description, 210) }}
                                        </p>

                                        <div style="margin-left:30px;margin-bottom:20px;margin-top:20px;">
                                            <a class="btn btn-secondary m-b-1" href="{{ route('blog.post', $post->slug) }}">Leer más</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        {{ $posts->links('vendor.pagination.bootstrap-4') }}
                    @else
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                <p>No hay artículos actualmente.</p>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-xs-3">
                    @if (count($other_posts))
                        <div class="card p-t-2 m-b-0">
                            <h6 style="padding-left:2rem">Últimos artículos</h6>
                            <ul class="post_list">
                                @foreach ($other_posts as $post)
                                    <li>
                                        <a class="clean" href="{{ route('blog.post', $post->slug) }}">
                                            {{ str_limit($post->title, 28) }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
