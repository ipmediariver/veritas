@extends('layouts.section')

@section('title', $post->title)

{{-- @section('breadcrumb-title', str_limit($post->title, 40)) --}}

@section('breadcrumb-list')
    <li class="breadcrumb-item"><a href="{{ url('/') }}">Inicio</a></li>
    <li class="breadcrumb-item"><a href="{{ route('blog') }}">Blog</a></li>
    <li class="breadcrumb-item active">{{ str_limit($post->title, 40) }}</li>
@endsection

@section('og-meta')
    <meta property="og:url"                content="{{ route('blog.post', $post->slug) }}" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="{{ $post->title }}" />
    <meta property="og:description"        content="{{ $post->description }}" />
    <meta property="og:image"              content="{{ asset($post->image) }}" />
@endsection

@section('section-content')
    <div class="servicios" id="servicios" style="background-image:url();">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <p>{{ str_limit($post->created_at, 10, '') }}</p>
                    <h2>{{ $post->title }}</h2>
                </div>
            </div>
        </div>

        <div class="section-cover-container container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section-cover" style="background-image: url({{ asset($post->image) }})"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-container container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12" style="font-size:16px;color:#000;">
                        {!! $post->content !!}
                    </div>
                </div>
            </div>
        </div>

        @if (count($other_posts))
            <div class="container">
                <div class="row">
                    <div class="offset-xs-0 col-xs-12 offset-sm-1 col-sm-10 offset-lg-2 col-lg-8 servicios-container flex flex-col">
                        <h2 class="text-xs-center m-b-3">Más de nuestros artículos</h2>
                    </div>
                </div>
            </div>

            <div class="servicios-carousel-div container-fluid">
                <div class="container">
                    <div class="row servicios-carousel m-b-0">
                        @foreach ($other_posts as $post)
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                <div class="card m-t-1">
                                    <div class="card-img" style="height:150px;background-image: url({{ asset($post->image) }})"></div>

                                    <h3 class="card-text" style="height:180px;">
                                        <a class="clean" href="{{ route('blog.post', $post->slug) }}">
                                            {{ str_limit($post->title, 45) }}
                                        </a>
                                    </h3>

                                    <div class="card-btn-container">
                                        <a class="btn btn-secondary" href="{{ route('blog.post', $post->slug) }}">Leer más</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
