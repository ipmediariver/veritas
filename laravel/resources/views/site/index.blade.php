@extends('layouts.master')

@section('title', 'Centro Veritas de Control y Confianza')

@section('content')
	@include('components.logo')
	@include('components.slider')
	@include('components.servicios')

	<div class="nosotros" id="nosotros">
        <div class="container-fluid">
            <div class="row">
                <div class="img-poligrafia col-xs-12 col-md-6" style="background-image: url({{ asset('assets/img/reclutamiento.jpg') }})"></div>
                <div class="p-x-3 col-xs-12 col-md-6 nosotros-container flex flex-left flex-col">
                    <h1 class="m-b-2">Quienes somos</h1>
                    <div class="row">
                        <p class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-xl-8">
                            El presente y futuro de una empresa está en sus colaboradores, por lo que es imperativo conocer a los nuevos integrantes o en su defecto determinar el compromiso y lealtad de los que ya se encuentran laborando. El poligrafo es utilizado en el sector privado para medir la lealtad, honestidad y confiabilidad de los trabajadores y los candidatos a ocupar los puestos vacantes.
                        </p>
                    </div>

                    <div class="row">
                        <p class="m-t-1 col-xs-12">
                            Así que asegure su proceso de selección
                        </p>
                    </div>

                    <a class="m-t-1 btn btn-lg btn-secondary" href="{{ route('nosotros') }}">Continuar leyendo</a>
                </div>
            </div>
        </div>
    </div>

	{{-- @include('components.testimonios') --}}
	@include('components.miembros')
	@include('components.contacto')
@endsection
