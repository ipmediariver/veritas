@extends('layouts.section')

@section('title', 'Contacto')

@section('breadcrumb-title', 'Contacto')

@section('breadcrumb-list')
    <li class="breadcrumb-item"><a href="{{ url('/') }}">Inicio</a></li>
    <li class="breadcrumb-item active">Contacto</li>
@endsection

@section('section-content')
    @include('components.contacto')
@endsection
