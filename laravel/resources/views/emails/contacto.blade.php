<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!--[if mso]>
            <style>
                * {
                    font-family: sans-serif !important;
                }
            </style>
        <![endif]-->
        <style>
            html,
            body {
                margin: 0 auto !important;
                padding: 0 !important;
                height: 100% !important;
                width: 100% !important;
            }

            * {
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%;
            }

            div[style*="margin: 16px 0"] {
                margin:0 !important;
            }

            table,
            td {
                mso-table-lspace: 0pt !important;
                mso-table-rspace: 0pt !important;
            }

            table {
                border-spacing: 0 !important;
                border-collapse: collapse !important;
                table-layout: fixed !important;
                margin: 0 auto !important;
            }
            table table table {
                table-layout: auto;
            }

            img {
                -ms-interpolation-mode:bicubic;
            }

            .mobile-link--footer a,
            a[x-apple-data-detectors] {
                color:inherit !important;
                text-decoration: underline !important;
            }

        </style>
    </head>

    <body width="100%" bgcolor="#ffffff" style="margin: 0;">
        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="max-width: 680px;">
            <tr>
                <td style="width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; color: #000000;">
                    <b>Nombre:</b> {{ $nombre }}<br>
                    <b>Correo electrónico:</b> {{ $correo }}<br>
                    <b>Empresa:</b> {{ $empresa }}<br>
                    <b>Teléfono:</b> {{ $telefono }}<br>
                    <b>Fecha:</b> {{ $fecha }}<br>
                    <b>Servicios solicitados:</b>
                    @foreach ($servicios as $servicio)
                        @if ($loop->last)
                            {{ $servicio }}<br>
                        @else
                            {{ $servicio }},&nbsp;
                        @endif
                    @endforeach
                    {!! $web ? '<b>Página web:</b> '.$web.'<br>' : '' !!}
                    <b>Mensaje:</b> {{ $mensaje }}
                </td>
            </tr>
        </table>
    </body>
</html>
