@extends('layouts.master')

@section('content')
    @include('components.breadcrumb')
    @yield('section-content')
@endsection
