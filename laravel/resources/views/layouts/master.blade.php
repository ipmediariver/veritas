<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>
			@yield('title'){{ \Request::route()->getPath() != '/' ? ' - Centro Veritas de Control y Confianza' : '' }}
		</title>
		<link rel="icon" type="image/png" href="img/favicon.png">
		<link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
		@yield('og-meta')

		<!-- Analytics -->
		<script>
		  	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		 	ga('create', 'UA-86875433-2', 'auto');
		 	ga('send', 'pageview');
		</script>
	</head>
	<body>
		@include('components.fb-sdk')

		@include('components.navbar')
		@include('components.navbar-fixed')
		@include('components.alert')
		@yield('content')
		@include('components.footer')
		@include('components.scripts')
	</body>
</html>
