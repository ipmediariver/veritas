<!DOCTYPE html>
<html class="no-js" lang="">
	<head>
		<meta charset="utf-8">
		<title>
			@yield('title'){{ \Request::route()->getPath() != '/' ? ' - Centro Veritas de Control y Confianza' : '' }}
		</title>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/chosen.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/jquery.tagsinput.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/checkBo.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/intlTelInput.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/daterangepicker.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/bootstrap-datepicker3.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/bootstrap-clockpicker.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/bootstrap-colorpicker.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/bootstrap3-wysihtml5.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/jquery.bootstrap-touchspin.min.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/jquery-labelauty.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/multi-select.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/select.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/select2.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/selectize.css') }}">

		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/vendor/jquery.dataTables.css') }}"/>

		<link rel="stylesheet" href="{{ asset('assets/dashboard/css/app.min.css') }}"/>
	</head>
	<body class="page-loading">
		@include('components.fb-sdk')

	    <div class="pageload">
	        <div class="pageload-inner">
	            <div class="sk-rotating-plane"></div>
	        </div>
	    </div>

	    <div class="app layout-fixed-header">

	        <div class="sidebar-panel offscreen-left">
	            <div class="brand">

	                <div class="toggle-offscreen">
	                    <a href="#" class="visible-xs hamburger-icon" data-toggle="offscreen" data-move="ltr">
	                        <span></span>
	                        <span></span>
	                        <span></span>
	                    </a>
	                </div>

	                <a class="brand-logo">
	                    <span>Veritas</span>
	                </a>

	            </div>

	            <nav role="navigation">
	                <ul class="nav">

						<li class="@yield('dashboard-view')">
					        <a href="{{ route('dashboard') }}">
					            <i class="icon-grid"></i>
					            <span>Dashboard</span>
					        </a>
					    </li>

						<li class="@yield('clientes-view')">
					        <a href="{{ route('dashboard.clientes') }}">
					            <i class="icon-user"></i>
					            <span>Clientes</span>
					        </a>
					    </li>

						<li class="@yield('reportes-view')">
					        <a href="{{ route('dashboard.reportes') }}">
					            <i class="icon-note"></i>
					            <span>Reportes</span>
					        </a>
					    </li>

						<li class="@yield('posts-view')">
					        <a href="{{ route('dashboard.posts') }}">
					            <i class="icon-book-open"></i>
					            <span>Artículos</span>
					        </a>
					    </li>
	                </ul>
	            </nav>

	        </div>


	        <div class="main-panel">

	            <div class="header navbar">
	                <div class="brand visible-xs">

	                    <div class="toggle-offscreen">
	                        <a href="#" class="hamburger-icon visible-xs" data-toggle="offscreen" data-move="ltr">
	                            <span></span>
	                            <span></span>
	                            <span></span>
	                        </a>
	                    </div>


	                    <a class="brand-logo">
	                        <span>Centro Veritas</span>
	                    </a>

	                </div>

					<ul class="nav navbar-nav hidden-xs">
						<li>
							<a href="{{ url('/') }}">
								<i class="icon-toggle-sidebar" style="margin-right:10px;"></i>Volver al sitio web
							</a>
						</li>
					</ul>

	                <ul class="nav navbar-nav navbar-right hidden-xs">

	                    <li>
	                        <a href="#" class="ripple" data-toggle="dropdown">
	                            <i class="icon-user" style="margin-right: 10px;"></i>
	                            <span>{{ Auth::user()->name }}</span>
	                            <span class="caret"></span>
	                        </a>
	                        <ul class="dropdown-menu">
	                            <li>
									<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
										{{ csrf_field() }}
									</form>
									<a href="{{ url('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Cerrar sesión</a>
	                            </li>
	                        </ul>
	                    </li>
	                </ul>
	            </div>


	            <div class="main-content">

					@yield('section-title')

	                @yield('content')

	            </div>

	        </div>


	        <footer class="content-footer" style="height: 2.6rem;">
	            <nav class="footer-left hidden-xs">
	                <ul class="nav">
	                    <li>
	                        <a href="{{ url('/') }}">Inicio</a>
	                    </li>
	                    <li>
	                        <a href="{{ route('servicios') }}">Servicios</a>
	                    </li>
	                    <li>
	                        <a href="{{ route('nosotros') }}">Nosotros</a>
	                    </li>
	                    <li>
	                        <a href="{{ route('contacto') }}">Contacto</a>
	                    </li>
	                </ul>
	            </nav>
	        </footer>
	    </div>

	    <script src="{{ asset('assets/dashboard/js/app.min.js') }}"></script>

	    <script src="{{ asset('assets/dashboard/js/vendor/jquery.dataTables.js') }}"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/bootstrap-datatables.js') }}"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/table-edit.js') }}"></script>

	    <script src="{{ asset('assets/dashboard/js/vendor/chosen.jquery.min.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/jquery.tagsinput.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/checkBo.min.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/intlTelInput.min.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/moment.min.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/daterangepicker.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/bootstrap-timepicker.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/jquery-clockpicker.min.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/bootstrap-colorpicker.min.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/jquery.bootstrap-touchspin.min.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/select2.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/selectize.min.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/jquery-labelauty.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/bootstrap-maxlength.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/typeahead.bundle.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/jquery.multi-select.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/bootstrap-datepicker.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/bootstrap-datepicker.js') }}" charset="utf-8"></script>
	    <script src="{{ asset('assets/dashboard/js/vendor/bootstrap3-wysihtml5.all.min.js') }}" charset="utf-8"></script>


	    <script src="{{ asset('assets/dashboard/js/vendor/plugins.js') }}" charset="utf-8"></script>

	    <script src="{{ asset('assets/dashboard/js/vendor/jquery.maskedinput.min.js') }}"></script>

	    <script src="{{ asset('assets/dashboard/js/vendor/masks.js') }}"></script>

		@yield('script')
	</body>
</html>
