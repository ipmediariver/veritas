@extends('layouts.section')

@section('title', 'Iniciar sesión')

@section('breadcrumb-title', 'Iniciar sesión')

@section('breadcrumb-list')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Inicio</a></li>
    <li class="breadcrumb-item active">Inicio de sesión</li>
@endsection

@section('section-content')
<div class="container p-y-3">
    <div class="row">
        <div class="col-md-6">
            <form class="" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="control-label">Correo electrónico</label>

                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="m-t-2 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="control-label">Contraseña</label>

                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember"><span class="m-l-1">Recordarme</span>
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Iniciar sesión
                    </button>

                    <!-- <a class="btn btn-link" href="{{ url('/password/reset') }}">
                        Forgot Your Password?
                    </a> -->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
