<div class="v-navbar-fixed">
    <div class="container">
        <div class="row">
            <div class="col-xs-3 col-md-3 flex flex-l nav-list">
                <a href="{{ url('/') }}" style="opacity:1;"><img class="logo img-fluid" src="{{ asset('assets/template/img/logo_veritas.svg') }}" alt="" /></a>
            </div>
            <div class="col-xs-9 col-md-9 flex flex-r nav-list">
                @if (\Request::route()->getPath() == '/')
                    <a href="#top">Inicio</a>
                    <a href="#servicios">Servicios</a>
                    <a href="#nosotros">Nosotros</a>
                    <a href="#contacto">Contacto</a>
                    <a href="{{ route('blog') }}">Blog</a>
                @else
                    <a href="{{ url('/') }}">Inicio</a>
                    <a href="{{ route('servicios') }}">Servicios</a>
                    <a href="{{ route('nosotros') }}">Nosotros</a>
                    <a href="{{ route('contacto') }}">Contacto</a>
                    <a href="{{ route('blog') }}">Blog</a>
                @endif
            </div>
        </div>
    </div>
</div>
