@if (session('message'))
    <div class="alert {{ session('class') }}">
        <button type="button" class="close" data-dismiss="alert">
            <span>&times;</span>
        </button>
        {{ session('message') }}
    </div>
@endif
