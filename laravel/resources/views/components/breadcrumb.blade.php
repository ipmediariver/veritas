<div class="breadcrumb-container container-fluid">
    <div class="container">
        <div class="breadcrumb-row row p-y-2">
            <div class="col-xs-6">
                <h4 class="m-b-0">@yield('breadcrumb-title')</h4>
            </div>
            <div class="col-xs-6">
                <ol class="breadcrumb pull-xs-right p-r-0">
                    @yield('breadcrumb-list')
                </ol>
            </div>
        </div>
    </div>
</div>
