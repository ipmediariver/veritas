<div class="veritas-logo-container container">
    <div class="row">
        <div class="col-xs-12">
            <div class="veritas-logo">
                <img src="{{ asset('assets/template/img/logo_veritas.svg') }}" alt="" />
            </div>
        </div>
    </div>
</div>

<style media="screen">
    .veritas-logo {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
        position: absolute;
        top: 80px;
        bottom: auto;
        left: auto;
        right: 0;
        z-index: 1;
    }
    .veritas-logo img {
        height: 300px;
    }

    @media (max-width: 991px) {
        .veritas-logo {
            display: none;
        }
    }
</style>
