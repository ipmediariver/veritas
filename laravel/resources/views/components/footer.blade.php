<footer>
    <div class="container">
        <div class="row m-b-3">
            <div class="m-t-3 col-xs-12 col-md-6 col-lg-5 col-xl-4">
                <h5>Centro veritas</h5>
                <p class="m-t-2">
                    {{ str_limit('El presente y futuro de una empresa está en sus colaboradores, por lo que es imperativo conocer a los nuevos integrantes o en su defecto determinar el compromiso y lealtad de los que ya se encuentran laborando.', 250)}}
                </p>
                <a class="arrow" href="{{ route('nosotros') }}">Leer más</a>
            </div>
            <div class="m-t-3 col-xs-12 col-md-6 col-lg-3 col-xl-2">
                <h5>Menú</h5>
                <ul class="arrow m-t-2">
                    <li><a href="{{ url('nosotros') }}">Nosotros</a></li>
                    <li><a href="{{ url('servicios') }}">Servicios</a></li>
                    <li><a href="{{ url('contacto') }}">Contacto</a></li>
                    @if(Auth::guest())
                        <li><a href="{{ route('login') }}">Iniciar sesión</a></li>
                    @else
                        <li><a href="{{ route('dashboard') }}">Administración</a></li>
                    @endif
                </ul>
            </div>
            <div class="m-t-3 col-xs-12 col-md-6 col-lg-4 col-xl-3">
                <h5>Servicios</h5>
                <ul class="arrow m-t-2">
                    <li><a href="#">Seguridad corporativa</a></li>
                    <li><a href="#">Corporaciones de seguridad</a></li>
                    <li><a href="#">Recursos humanos</a></li>
                </ul>
            </div>
            <div class="m-t-3 col-xs-12 col-md-6 col-lg-12 col-xl-3">
                <h5>Comunícate con nosotros</h5>
                <ul class="m-t-2">
                    <li><i class="fa fa-map-marker"></i>Blvd. Agua Caliente 4558 C1 L 32-A<br><span class="m-l-1">22014 Tijuana, B.C. México</span></li>
                    <li><a href="tel:664-124-7845"><i class="fa fa-phone"></i>(664) 124 7845</a></li>
                    <li><a href="mailto:info@centroveritas.com"><i class="fa fa-envelope"></i>info@centroveritas.com</a></li>
                    <!-- <li><i class="fa fa-clock-o"></i>Lunes - Viernes: 9:00AM - 5:00PM<br>
                        <span style="margin-right:22px;"></span>Sábado - Domingo: Cerrado</li> -->
                </ul>
            </div>
        </div>
    </div>

    <hr>

    <div class="container">
        <div class="row sub-footer">
            <div class="col-xs-12 col-md-6">
                <p class="text-xs-center text-md-left">
                    Desarrollo de: IP Media River
                </p>
            </div>
            <div class="col-xs-12 col-md-6">
                <p class="text-xs-center text-md-right">
                    &copy; 2016, Centro Veritas de Control y Confianza.
                </p>
            </div>
        </div>
    </div>
</footer>
