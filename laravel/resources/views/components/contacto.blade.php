<style media="screen">
    input[type=date]::-webkit-inner-spin-button,
    input[type=date]::-webkit-outer-spin-button,
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
</style>
<div class="contacto-container container-fluid" id="contacto">
    <div class="container">
        <div class="row">
            <div class="offset-xs-0 col-xs-12 offset-sm-1 col-sm-10 offset-lg-3 col-lg-6 testimonios-container flex flex-col">
                <h2 class="text-xs-center m-b-2">Solicita nuestros servicios</h2>
                <!-- <p class="text-xs-center">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididtion ullamco luptate velit esse fugiat nulla pariatur.
                </p> -->
            </div>
        </div>

        <div class="row" style="margin-top: 60px;">
            <div class="col-xs-12 offset-xs-0 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
                <form method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label>Nombre completo</label> <span class="small text-danger">*</span>
                                <input class="form-control" type="text" name="name" value="{{ old('name') }}" required>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label>Correo electrónico</label> <span class="small text-danger">*</span>
                                <input class="form-control" type="email" name="email" value="{{ old('email') }}" required>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label>Empresa</label> <span class="small text-danger">*</span>
                                <input class="form-control" type="text" name="company" value="{{ old('company') }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-1">
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label>Teléfono</label> <span class="small text-danger">*</span>
                                <input class="form-control" type="tel" name="phone" value="{{ old('phone') }}" required>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label>Fecha</label> <span class="small text-danger">*</span>
                                <input class="form-control" type="date" name="date" value="{{ old('date') }}" required>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label>Página Web</label>
                                <input class="form-control" type="text" name="web" value="{{ old('web') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Servicios</label> <span class="small text-danger">*</span><br>
                                <input id="servicio-1" class="contacto-btn btn btn-secondary btn-lg" type="button" value="Prueba exploratoria">
                                <input id="servicio-2" class="contacto-btn btn btn-secondary btn-lg" type="button" value="Prueba de permanencia laboral">
                                <input id="servicio-3" class="contacto-btn btn btn-secondary btn-lg" type="button" value="Prueba específica">
                                <input id="servicio-4" class="contacto-btn btn btn-secondary btn-lg" type="button" value="Prueba psicométrica">
                                <input id="servicio-5" class="contacto-btn btn btn-secondary btn-lg" type="button" value="Prueba grafológica">
                                <input id="servicio-6" class="contacto-btn btn btn-secondary btn-lg" type="button" value="Prueba EyeDetect">
                                <input id="servicio-7" class="contacto-btn btn btn-secondary btn-lg" type="button" value="Estudio socioeconómico">

                                <select id="services" name="services[]" multiple hidden>
                                    <option id="servicio-1" value="Prueba exploratoria"></option>
                                    <option id="servicio-2" value="Prueba de permanencia laboral"></option>
                                    <option id="servicio-3" value="Prueba específica"></option>
                                    <option id="servicio-4" value="Prueba psicométrica"></option>
                                    <option id="servicio-5" value="Prueba grafológica"></option>
                                    <option id="servicio-6" value="Prueba EyeDetect"></option>
                                    <option id="servicio-7" value="Estudio socioeconómico"></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-t-1">
                        <label>Mensaje</label> <span class="small text-danger">*</span>
                        <textarea class="form-control" name="message" rows="8" cols="40" required>{{ old('message') }}</textarea>
                    </div>

                    <button type="submit" class="m-t-2 btn btn-lg btn-primary">Enviar</button>
                </form>
            </div>
        </div>
    </div>

</div>
