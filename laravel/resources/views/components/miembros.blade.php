<div class="miembro-container container-fluid" id="miembro">
    <div class="container">
        <div class="row">
            <div class="offset-xs-0 col-xs-12 offset-sm-1 col-sm-10 offset-lg-2 col-lg-8 servicios-container flex flex-col">
                <h2 class="text-xs-center title">Miembro activo de:</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 offset-md-1 col-md-10">
                <ul class="miembro-list">
                    <li class="miembro-item"><img src="{{ asset('assets/img/miembro/aapp.gif') }}" alt="" /></li>
                    <li class="miembro-item"><img src="{{ asset('assets/img/miembro/aipp.png') }}" alt="" /></li>
                    <li class="miembro-item"><img src="{{ asset('assets/img/miembro/alp.png') }}" alt="" /></li>
                    <li class="miembro-item"><img src="{{ asset('assets/img/miembro/apa.png') }}" alt="" /></li>
                </ul>
            </div>
        </div>
    </div>
</div>
