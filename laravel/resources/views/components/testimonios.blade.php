<div class="testimonios" id="testimonios">
    <div class="container">
        <div class="row">
            <div class="offset-xs-0 col-xs-12 offset-sm-1 col-sm-10 offset-lg-3 col-lg-6 testimonios-container flex flex-col">
                <h2 class="text-xs-center m-b-2">Testimonios de clientes</h2>
                <p class="text-xs-center">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididtion ullamco luptate velit esse fugiat nulla pariatur.
                </p>
            </div>
        </div>
        <div class="testimonios-carousel row">
            <div class="col-xs-12 flex m-b-3 m-t-1">
                <div class="testimonios-card">
                    <p class="text-xs-center m-a-0">
    					Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis Red quia numquam eius modi. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
    				</p>
                </div>
                <p class="testimonios-nombre text-xs-center m-t-3">
                    Agustín Abaroa<br>
                    (CEO)
                </p>
            </div>
            <div class="col-xs-12 flex m-b-3 m-t-1">
                <div class="testimonios-card">
                    <p class="text-xs-center m-a-0">
    					Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis Red quia numquam eius modi. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
    				</p>
                </div>
                <p class="testimonios-nombre text-xs-center m-t-3">
                    Andrés Farfán<br>
                    (CEO)
                </p>
            </div>
            <div class="col-xs-12 flex m-b-3 m-t-1">
                <div class="testimonios-card">
                    <p class="text-xs-center m-a-0">
    					Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis Red quia numquam eius modi. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
    				</p>
                </div>
                <p class="testimonios-nombre text-xs-center m-t-3">
                    Agustín Abaroa<br>
                    (CEO)
                </p>
            </div>
            <div class="col-xs-12 flex m-b-3 m-t-1">
                <div class="testimonios-card">
                    <p class="text-xs-center m-a-0">
    					Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipis Red quia numquam eius modi. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur
    				</p>
                </div>
                <p class="testimonios-nombre text-xs-center m-t-3">
                    Andrés Farfán<br>
                    (CEO)
                </p>
            </div>
        </div>
    </div>
</div>
