<script src="{{ asset('assets/js/jquery-3.0.0.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('assets/js/tether-1.2.0.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('assets/js/slick.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('assets/js/scroll2id.js') }}" charset="utf-8"></script>
<script src="{{ asset('assets/js/app.js') }}" charset="utf-8"></script>
