<div class="slider-carousel">
    <div class="slider">
        <div class="container">
            <div class="row">
                <div class="slider-container col-xs-12 col-md-8 col-lg-6 flex flex-col flex-left">
                    <h2 style="font-size: 23px" class="item-1 bkg-black">Soluciones de Control de Confianza</h2>
                    <h5 style="line-height: 28px; letter-spacing: .05rem;" class="item-2 bkg-blue">Para el Sector Privado y el Sector Público</h5>
                    <!-- <p class="item-3 m-t-1">
                        Identificar, mitigar y administrar efectivamente riesgos y vulnerabilidades que puedan amenazar la seguridad de la empresa, y la supervivencia de la organización.
                    </p> -->
                </div>
            </div>
        </div>
    </div>
    <div class="slider bg-y-4" style="background-image: url({{ asset('assets/img/pruebas/sector_privado.jpg') }})">
        <div class="container">
            <div class="row">
                <div class="slider-container col-xs-12 col-md-8 col-lg-6 flex flex-col flex-left">
                    <h2 style="font-size: 23px" class="item-1 bkg-black">Soluciones para el Sector Privado</h2>
                    <h5 style="line-height: 28px; letter-spacing: .05rem;" class="item-2 bkg-blue">Prueba Exploratoria o de Nuevo Ingreso <br> Permanencia Laboral <br> Estudios de Control y Confianza</h5>
                    <!-- <p class="item-3 m-t-1">
                        Las nuevas tecnologías aplicadas al ámbito de la seguridad permiten un mayor control sobre los procesos que las corporaciones de seguridad llevan a cabo al momento de operar.
                    </p> -->
                </div>
            </div>
        </div>
    </div>
    <div class="slider bg-y-8" style="background-image: url({{ asset('assets/img/recursos-humanos.jpg') }})">
        <div class="container">
            <div class="row">
                <div class="slider-container col-xs-12 col-md-8 col-lg-6 flex flex-col flex-left">
                    <h2 style="font-size: 23px" class="item-1 bkg-black">Soluciones para el Sector Público</h2>
                    <h5 style="line-height: 28px; letter-spacing: .05rem;" class="item-2 bkg-blue">
                    Prueba Exploratoria o de Nuevo Ingreso <br> Permanencia Laboral <br> Estudios de Control y Confianza</h5>
                    <!-- <p class="item-3 m-t-1">
                        El presente y futuro de una empresa está en sus colaboradores, por lo que es imperativo conocer a los nuevos integrantes o en su defecto determinar el compromiso y lealtad de los que ya se encuentran laborando.
                    </p> -->
                </div>
            </div>
        </div>
    </div>
    <div class="slider bg-y-5" style="background-image: url({{ asset('assets/img/pruebas/prueba-eyedetect.jpg') }})">
        <div class="container">
            <div class="row">
                <div class="slider-container col-xs-12 col-md-8 col-lg-6 flex flex-col flex-left">
                    <h2 style="font-size: 23px" class="item-1 bkg-black">Tecnología EyeDetect</h2>
                    <h5 style="line-height: 28px; letter-spacing: .05rem;" class="item-2 bkg-blue">Prueba Rápida para Detectar Mentiras por Medio del Análisis del comportamiento de los ojos.</h5>
                    <!-- <p class="item-3 m-t-1">
                        Tecnología precisa y no invasiva que detecta mentiras por medio del análisis del comportamiento de los ojos en una prueba de 30 minutos.
                    </p> -->
                </div>
            </div>
        </div>
    </div>
    <div class="slider bg-y-8" style="background-image: url({{ asset('assets/img/pruebas/poligrafia.jpg') }})">
        <div class="container">
            <div class="row">
                <div class="slider-container col-xs-12 col-md-8 col-lg-6 flex flex-col flex-left">
                    <h2 style="font-size: 23px" class="item-1 bkg-black">Poligrafía</h2>
                    <h5 style="line-height: 28px; letter-spacing: .05rem;" class="item-2 bkg-blue">Herramienta que te ayuda a determinar la veracidad de la información proporcionada por un individuo</h5>
                    <!-- <p class="item-3 m-t-1">
                        El presente y futuro de una empresa está en sus colaboradores, por lo que es imperativo conocer a los nuevos integrantes o en su defecto determinar el compromiso y lealtad de los que ya se encuentran laborando.
                    </p> -->
                </div>
            </div>
        </div>
    </div>
</div>
