<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/', function () { return view('site.index'); });
Route::post('/', 'MailController@contacto');

Route::get('contacto', function () { return view('site.contacto'); })->name('contacto');
Route::post('contacto', 'MailController@contacto');

Route::get('nosotros', function () { return view('site.nosotros'); })->name('nosotros');

Route::group(['prefix' => 'blog'], function () {
    Route::get('/', 'PostController@index')->name('blog');
    Route::get('/{slug}', 'PostController@post')->name('blog.post');
});

Route::group(['prefix' => 'servicios'], function () {
    Route::get('/', 'ServicioController@index')->name('servicios');
    Route::get('prueba-exploratoria', 'ServicioController@prueba_exploratoria')->name('prueba.exploratoria');
    Route::get('prueba-de-permanencia-laboral', 'ServicioController@prueba_permanencia')->name('prueba.permanencia');
    Route::get('prueba-especifica', 'ServicioController@prueba_especifica')->name('prueba.especifica');
    Route::get('prueba-psicometrica', 'ServicioController@prueba_psicometrica')->name('prueba.psicometrica');
    Route::get('prueba-grafologica', 'ServicioController@prueba_grafologica')->name('prueba.grafologica');
    Route::get('prueba-eyedetect', 'ServicioController@prueba_eyedetect')->name('prueba.eyedetect');
    Route::get('estudio-socioeconomico', 'ServicioController@estudio_socioeconomico')->name('estudio.socioeconomico');
});

Route::group(['middleware' => 'auth', 'prefix' => 'dashboard'], function () {
    Route::get('/', 'HomeController@index')->name('dashboard');

    Route::group(['namespace' => 'Dashboard'], function () {
        Route::resource('clientes', 'ClienteController', ['names' => ['index' => 'dashboard.clientes']]);
        Route::get('clientes/{id}/delete', 'ClienteController@destroy')->name('clientes.delete');
        
        Route::resource('reportes', 'ReporteController', ['names' => ['index' => 'dashboard.reportes']]);
        Route::get('reportes/{id}/delete', 'ReporteController@destroy')->name('reportes.delete');

        Route::resource('posts', 'PostController', ['names' => ['index' => 'dashboard.posts']]);
        Route::get('posts/{id}/publish', 'PostController@publish')->name('posts.publish');
        Route::get('posts/{id}/delete', 'PostController@destroy')->name('posts.delete');
    });
});
